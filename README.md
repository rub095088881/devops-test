# DevOps Engineer - Test Assignment 

This is a test assignment for the role of DevOps Engineer.

## Objectives

This test will help us understand:

- the way you approach infrastructure design
- the way you deal with security implications

We believe in automation, and we think infrastructure must be represented as code.

In this test assignment, you have to create a Gitlab CI pipeline which deploys simple web application. Terraform must also be used as an infrastructure as code tool and Docker as a containerization. At your discretion you may also additionally use Ansible, Bash, etc. You may use either AWS or GCP as a target cloud.

## Submit your results

As a result, we expect to see a working pipeline which includes terraform plan and apply stages. You may create Gitlab repository, or you can send us a zip archive with your git repository. We expect meaningful git commits which properly describe what they do. Also please commit as often as you can - we would prefer to see your history of trial and error. 
