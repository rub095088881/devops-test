

#Create key-pair for logging into EC2 in us-east-1
resource "aws_key_pair" "master-key" {
  #provider   = aws.region-master
  key_name   = "nodeapp-key"
  public_key = file("~/.ssh/id_rsa.pub")
}



#Create and bootstrap EC2 in us-east-1
resource "aws_instance" "nodeapp-master" {
  #provider                    = aws.region-master
  ami                         = "ami-0149b2da6ceec4bb0"
  instance_type               = var.instance-type
  key_name                    = aws_key_pair.master-key.key_name
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.nodeapp-sg.id]
  subnet_id                   = aws_subnet.subnet_1.id
  user_data                   = file("user-data.sh")

  tags = {
    Name = "nodeapp_master_tf"
  }
  depends_on = [aws_main_route_table_association.set-master-default-rt-assoc]
}
